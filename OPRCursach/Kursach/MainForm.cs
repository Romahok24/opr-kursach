﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kursach
{
    public partial class MainForm : Form
    {
        XMLEditer editer = new XMLEditer("items.xml");
        Algorithm algorithm = new Algorithm();

        public MainForm()
        {
            InitializeComponent();
            algorithm.Items = editer.Read();
            dataGridView1.DataSource = algorithm.Items;
            algorithm.Capacity = Convert.ToInt32(backpacktextBox.Text);
            dataGridView2.DataSource = algorithm.ItemsInPack;
        }

        private void SolveButtonClick(object sender, EventArgs e)
        {
            algorithm.ItemsInPack.Clear();

            algorithm.Solve();

            dataGridView2.DataSource = algorithm.ItemsInPack;

            label7.Text = $"Общая стоимость: {algorithm.TotalValue}";
            label8.Text = $"Общий вес: {algorithm.TotalWeight}";
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            Save("itemsInPack.xml", algorithm.ItemsInPack.ToList());
        }

        private void LoadButtonClick(object sender, EventArgs e)
        {
            algorithm.Items.Clear();
            algorithm.Items = (BindingList<Item>)editer.Read();
        }

        private void SetCapacityClick(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(backpacktextBox.Text))
                algorithm.Capacity = Convert.ToInt32(backpacktextBox.Text);
            else
                MessageBox.Show("Входная строка имеет не верный формат!");
        }

        private void AddButtonClick(object sender, EventArgs e)
        {
            var name = nameTextBox.Text;
            var weight = Convert.ToInt32(weightTextBox.Text);
            var count = Convert.ToInt32(countTextBox.Text);
            var value = Convert.ToInt32(valueTextBox.Text);
            var item = algorithm.Items.FirstOrDefault(x => x.Name.Equals(name)); 
            
            if (item != null)
            {
                if (item.Weight == weight)
                    item.Quantity += count;
                else
                {
                    name = $"{name} ({algorithm.Items.Where(x => x.Name.Contains(name)).Count() + 1})";
                    algorithm.Items.Add(new Item(name, weight, value, count));
                }
            }
            else
                algorithm.Items.Add(new Item(name, weight, value, count));

            nameTextBox.Text = countTextBox.Text = valueTextBox.Text = weightTextBox.Text = String.Empty;
            
            dataGridView1.Refresh();
        }

        private void Save(string path, List<ItemInPack> list)
        {
            editer.Write(list, algorithm.TotalWeight, algorithm.TotalValue, path);
            MessageBox.Show("Сохранено");
        }

        private void saveAnotherButton_Click(object sender, EventArgs e)
        {
            Save("anotherItems.xml", algorithm.AnotherItems.ToList());
        }

        private void СhangeClick(object sender, EventArgs e)
        {
            var name = nameTextBox.Text;
            var item = algorithm.Items.FirstOrDefault(x => x.Name.Equals(name));

            if (item != null)
            {
                item.Quantity = Convert.ToInt32(countTextBox.Text);
                item.Value = Convert.ToInt32(valueTextBox.Text);
                item.Weight = Convert.ToInt32(weightTextBox.Text);
            }
            else
                AddButtonClick(sender, e);

            nameTextBox.Text = countTextBox.Text = valueTextBox.Text = weightTextBox.Text = String.Empty;
            
            addButton.Visible = true;
            changeButton.Visible = true;

            dataGridView1.Refresh();
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var row = dataGridView1.SelectedRows[0];

            nameTextBox.Text = row.Cells[0].Value.ToString();
            weightTextBox.Text = row.Cells[1].Value.ToString();
            valueTextBox.Text = row.Cells[2].Value.ToString();
            countTextBox.Text = row.Cells[3].Value.ToString();

            addButton.Visible = false;
            changeButton.Visible = true;
        }
    }
}
