﻿namespace Kursach
{
    public class Item
    {
        public string Name { get; set; }

        public int Weight { get; set; }

        public int Value { get; set; }

        public int Quantity { get; set; }

        public Item() { }

        public Item(string _name, int _weigth, int _value, int _quantity)
        {
            Name = _name;
            Weight = _weigth;
            Value = _value;
            Quantity = _quantity;
        }
    }
}
