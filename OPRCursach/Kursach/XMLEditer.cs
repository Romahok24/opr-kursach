﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
using System.Xml.Linq;

namespace Kursach
{
    public class XMLEditer
    {
        public string path = string.Empty;
        public XMLEditer(string _path)
        {
            path = _path;
        }

        public BindingList<Item> Read()
        {
            BindingList<Item> items = new BindingList<Item>();
            XmlDocument doc = new XmlDocument();

            doc.Load(path);

            XmlElement root = doc.DocumentElement;
            foreach (XmlNode node in root)
            {
                var item = new Item();

                if (node.Attributes.Count > 0)
                {
                    XmlNode attr = node.Attributes.GetNamedItem("name");

                    if (attr != null)
                        item.Name = attr.Value;
                }

                foreach (XmlNode child in node.ChildNodes)
                {
                    if (child.Name.Equals("value"))
                        item.Value = Convert.ToInt32(child.InnerText);

                    if (child.Name.Equals("weight"))
                        item.Weight = Convert.ToInt32(child.InnerText);

                    if (child.Name.Equals("quantity"))
                        item.Quantity = Convert.ToInt32(child.InnerText);
                }

                items.Add(item);
            }

            return items;
        }

        public void Write(IEnumerable<Item> items, string path = null)
        {
            XDocument doc = new XDocument();
            XElement root = new XElement("items");

            foreach (var item in items)
            {
                XElement element = new XElement("item");

                element.Add(new XAttribute("name", item.Name));
                element.Add(new XElement("value", item.Value));
                element.Add(new XElement("weight", item.Weight));
                element.Add(new XElement("quantity", item.Quantity));

                root.Add(element);
            }

            doc.Add(root);
            doc.Save(path?? this.path);
        }

        public void Write(IEnumerable<ItemInPack> items,
            int totalWeight,int totalValue, string path)
        {
            XDocument doc = new XDocument();
            XElement root = new XElement("items");

            root.Add(new XAttribute("totalWeight", totalWeight));
            root.Add(new XAttribute("totalValue", totalValue));

            foreach (var item in items)
            {
                XElement element = new XElement("item");     
                
                element.Add(new XElement("name", item.Name));
                element.Add(new XElement("count", item.Count));

                root.Add(element);
            }

            doc.Add(root);
            doc.Save(path);
        }
    }
}
