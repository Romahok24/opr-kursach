﻿namespace Kursach
{
    public class ItemInPack
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
