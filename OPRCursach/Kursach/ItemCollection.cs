﻿using System.Collections.Generic;

namespace Kursach
{
    public class ItemCollection
    {
        public Dictionary<string, int> Contents = new Dictionary<string, int>();

        public int TotalValue;
        public int TotalWeight;

        public void AddItem(Item item, int quantity)
        {
            if (Contents.ContainsKey(item.Name)) 
                Contents[item.Name] += quantity; 
            else 
                Contents[item.Name] = quantity;

            TotalValue += quantity * item.Value;
            TotalWeight += quantity * item.Weight;
        }

        public ItemCollection Copy()
        {
            var ic = new ItemCollection();

            ic.Contents = new Dictionary<string, int>(this.Contents);
            ic.TotalValue = this.TotalValue;
            ic.TotalWeight = this.TotalWeight;

            return ic;
        }
    }
}
