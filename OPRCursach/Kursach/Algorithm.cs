﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Kursach
{
    class Algorithm
    {
        public int Capacity { get; set; }
        public int TotalValue { get; set; }
        public int TotalWeight { get; set; }

        public BindingList<Item> Items { get; set; } = new BindingList<Item>();
        public BindingList<ItemInPack> ItemsInPack { get; set; } = new BindingList<ItemInPack>();
        public BindingList<ItemInPack> AnotherItems { get; set; } = new BindingList<ItemInPack>();

        public void Solve()
        {
            ItemCollection[] ic = new ItemCollection[Capacity + 1];

            for (int i = 0; i <= Capacity; i++)
                ic[i] = new ItemCollection();

            for (int i = 0; i < Items.Count; i++)
            {
                for (int j = Capacity; j >= 0; j--)
                {
                    if (j >= Items[i].Weight)
                    {
                        int quantity = Math.Min(Items[i].Quantity, j / Items[i].Weight);

                        for (int k = 1; k <= quantity; k++)
                        {
                            ItemCollection lighterCollection = ic[j - k * Items[i].Weight];
                            int testValue = lighterCollection.TotalValue + k * Items[i].Value;

                            if (testValue > ic[j].TotalValue)
                                (ic[j] = lighterCollection.Copy()).AddItem(Items[i], k);
                        }
                    }
                }
            }

            ItemsInPack = InitList(ic, Capacity, ic.Length);
            AnotherItems = InitList(ic, 0, ic.Length - 1);

            TotalValue = ic[Capacity].TotalValue;
            TotalWeight = ic[Capacity].TotalWeight;
        }

        private BindingList<ItemInPack> InitList(ItemCollection[] collections, int start, int lenght)
        {
            BindingList<ItemInPack> list = new BindingList<ItemInPack>();

            for (int i = start; i < lenght; i++)
            {
                foreach (KeyValuePair<string, int> kvp in collections[i].Contents)
                    list.Add(new ItemInPack { Name = kvp.Key, Count = kvp.Value });
            }

            return list;
        }
    }
}
